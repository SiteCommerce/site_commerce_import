<?php

namespace Drupal\site_commerce_import\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'site_commerce_import.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'site_commerce_import_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * Реализует получения списка доступных плагинов импорта товаров и категорий.
   * @return array
   */
  protected function getPluginList() {
    $definitions = \Drupal::service('plugin.manager.site_commerce_import.excel')->getDefinitions();
    $plugin_list = [];
    foreach ($definitions as $plugin_id => $plugin) {
      $plugin_list[$plugin_id] = $this->t($plugin['label']->render());
    }
    return $plugin_list;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Загружаем конфигурацию.
    $config = $this->config(static::SETTINGS);

    $scheme = \Drupal::config('system.file')->get('default_scheme');

    $form['paths'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Import settings from Excel'),
    ];

    $form['paths']['import_plugin'] = [
      '#title' => $this->t('Select the plugin for importing'),
      '#type' => 'select',
      '#options' => $this->getPluginList(),
      '#empty_option' => $this->t('None'),
      '#default_value' => $config->get('import_plugin') ? $config->get('import_plugin') : "site_commerce_import_default_import_excel",
    ];

    // Путь до файла импорта с категориями каталога.
    $form['paths']['file_catalog_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path of catalog file'),
      '#default_value' => $config->get('file_catalog_path') ? $config->get('file_catalog_path') : $scheme . '://' . "import/excel/catalog.xls",
    ];

    $queue = \Drupal::queue('site_commerce_import_catalog_import');
    if ($number_of_items = $queue->numberOfItems()) {
      $form['paths']['info_text_catalog_import'] = [
        '#type' => 'markup',
        '#markup' => $this->t('In the queue for processing: @number.', [
          '@number' => $number_of_items,
        ]),
      ];
    }

    // Путь до файла импорта с товарами.
    $form['paths']['file_products_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path of products file'),
      '#default_value' => $config->get('file_products_path') ? $config->get('file_products_path') : $scheme . '://' . "import/excel/products.xls",
    ];

    $queue = \Drupal::queue('site_commerce_import_products_import');
    if ($number_of_items = $queue->numberOfItems()) {
      $form['paths']['info_text_products_import'] = [
        '#type' => 'markup',
        '#markup' => $this->t('In the queue for processing: @number.', [
          '@number' => $number_of_items,
        ]),
      ];
    }

    // Разрешить импорт категорий каталога.
    $form['paths']['allow_import_catalog'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow import of catalog categories'),
      '#default_value' => $config->get('allow_import_catalog'),
    ];

    // Разрешить импорт товаров.
    $form['paths']['allow_import_products'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow import of products'),
      '#default_value' => $config->get('allow_import_products'),
    ];

    // Разрешить повторный импорт файлов.
    $form['paths']['allow_re_importing_files'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow re-importing files'),
      '#default_value' => $config->get('allow_re_importing_files'),
      '#description' => $this->t('This option should be disabled by default. If this option is selected, files will be imported every time this form is saved, when CRON or web service requests are made to start the import.'),
    ];

    // Настройки импорта ссылок.
    $form['url'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Link import settings'),
    ];
    // Путь до файла импорта с URL ссылками на категории каталога.
    $form['url']['file_catalog_url_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path of catalog file'),
      '#default_value' => $config->get('file_catalog_url_path') ? $config->get('file_catalog_url_path') : $scheme . '://' . "import/excel/catalog_url.xls",
    ];
    // Путь до файла импорта с URL ссылками на товары.
    $form['url']['file_products_url_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path of products file'),
      '#default_value' => $config->get('file_products_url_path') ? $config->get('file_products_url_path') : $scheme . '://' . "import/excel/products_url.xls",
    ];
    // Разрешить импорт ссылок.
    $form['url']['allow_import_links'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow import of links'),
      '#default_value' => $config->get('allow_import_links'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Записывает значения в конфигурацию.
    $config = $this->configFactory->getEditable(static::SETTINGS);

    // Сохраняем название плагина для импорта.
    $import_plugin = trim($form_state->getValue('import_plugin'));
    $config->set('import_plugin', $import_plugin);

    // Путь до файла импорта с категориями каталога.
    $file_catalog_path = trim($form_state->getValue('file_catalog_path'));
    $config->set('file_catalog_path', $file_catalog_path);

    // Путь до файла импорта с товарами.
    $file_products_path = trim($form_state->getValue('file_products_path'));
    $config->set('file_products_path', $file_products_path);

    // Разрешить импорт категорий каталога.
    $allow_import_catalog = (int) $form_state->getValue('allow_import_catalog');
    $config->set('allow_import_catalog', $allow_import_catalog);

    // Разрешить импорт товаров.
    $allow_import_products = (int) $form_state->getValue('allow_import_products');
    $config->set('allow_import_products', $allow_import_products);

    // Разрешить повторный импорт файлов.
    $allow_re_importing_files = (int) $form_state->getValue('allow_re_importing_files');
    $config->set('allow_re_importing_files', $allow_re_importing_files);

    // Путь до файла импорта с URL ссылками на категории каталога.
    $file_catalog_url_path = trim($form_state->getValue('file_catalog_url_path'));
    $config->set('file_catalog_url_path', $file_catalog_url_path);

    // Путь до файла импорта с URL ссылками на товары.
    $file_products_url_path = trim($form_state->getValue('file_products_url_path'));
    $config->set('file_products_url_path', $file_products_url_path);

    // Разрешить импорт ссылок.
    $allow_import_links = (int) $form_state->getValue('allow_import_links');
    $config->set('allow_import_links', $allow_import_links);

    $config->save();

    $this->startImport();

    parent::submitForm($form, $form_state);
  }

  /**
   * Реализует создание очередей импорта.
   */
  private function startImport() {
    // Загружаем конфигурацию.
    $config = $this->config(static::SETTINGS);

    // Получаем текущую очередь выполнения импорта категорий.
    $queue = \Drupal::queue('site_commerce_import_excel_catalog_import');
    $number_of_catalog_items = $queue->numberOfItems();

    // Получаем текущую очередь выполнения импорта товаров.
    $queue = \Drupal::queue('site_commerce_import_excel_products_import');
    $number_of_products_items = $queue->numberOfItems();

    // Если отсутствует очередь создания категорий и товаров и существует файл импорта категорий.
    $file = $config->get('file_catalog_path');
    if (file_exists($file) && !$number_of_catalog_items && !$number_of_products_items && $config->get('allow_import_catalog')) {
      // Проверяем менялся ли файл импорта с момента последнего полного импорта.
      // Вычисляем его хэш.
      $md5_current = md5_file($file);

      $md5_last = $config->get('file_catalog_md5');

      // Файл обновлен, создаем новое задание на импорт.
      if ($md5_current != $md5_last || $config->get('allow_re_importing_files')) {
        \Drupal::service('site_commerce_import.excel')->createQueueCatalog();
      }
    }

    // Если отсутствует очередь создания категорий и товаров и существует файл импорта товаров.
    $file = $config->get('file_products_path');
    if (file_exists($file) && !$number_of_products_items && !$number_of_catalog_items && $config->get('allow_import_products')) {
      // Проверяем менялся ли файл импорта с момента последнего полного импорта.
      // Вычисляем его хэш.
      $md5_current = md5_file($file);

      $md5_last = $config->get('file_products_md5');

      // Файл обновлен, создаем новое задание на импорт.
      if ($md5_current != $md5_last || $config->get('allow_re_importing_files')) {
        \Drupal::service('site_commerce_import.excel')->createQueueProducts();
      }
    }

    // Получаем текущую очередь выполнения импорта URL ссылок категорий.
    $queue = \Drupal::queue('site_commerce_import_excel_catalog_url_import');
    $number_of_catalog_url_items = $queue->numberOfItems();

    // Получаем текущую очередь выполнения импорта URL ссылок товаров.
    $queue = \Drupal::queue('site_commerce_import_excel_products_url_import');
    $number_of_products_url_items = $queue->numberOfItems();

    // Если отсутствует очередь создания URL ссылок категорий.
    $file = $config->get('file_catalog_url_path');
    if (file_exists($file) && !$number_of_catalog_url_items && !$number_of_catalog_items && $config->get('allow_import_links')) {
      \Drupal::service('site_commerce_import.excel')->createQueueCatalogUrl();
    }

    // Если отсутствует очередь создания URL ссылок товаров.
    $file = $config->get('file_products_url_path');
    if (file_exists($file) && !$number_of_products_url_items && !$number_of_products_items && $config->get('allow_import_links')) {
      \Drupal::service('site_commerce_import.excel')->createQueueProductsUrl();
    }
  }
}

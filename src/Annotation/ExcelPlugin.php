<?php

namespace Drupal\site_commerce_import\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Annotations for ExcelPlugin.
 *
 * @Annotation
 */
class ExcelPlugin extends Plugin {

  /**
   * The plugin ID.
   */
  public $id;

  /**
   * Label will be used in interface.
   */
  public $label;

}

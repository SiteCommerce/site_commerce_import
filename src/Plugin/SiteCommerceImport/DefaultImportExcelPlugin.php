<?php

namespace Drupal\site_commerce_import\Plugin\SiteCommerceImport;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\site_commerce_import\Annotation\ExcelPlugin;
use Drupal\site_commerce_import\ExcelPluginBase;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\TermInterface;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use Drupal\path_alias\PathAliasInterface;
use Drupal\site_commerce_product\Entity\ProductInterface;

/**
 * Class DefaultImportExcelPlugin
 * @package Drupal\site_commerce_import\Plugin\SiteCommerceImport\DefaultImportExcelPlugin
 *
 * @ExcelPlugin(
 *   id = "site_commerce_import_default_import_excel",
 *   label = @Translation("Default")
 * )
 */
class DefaultImportExcelPlugin extends ExcelPluginBase {

  use StringTranslationTrait;
  use MessengerTrait;

  /**
   * Import settings configuration
   */
  private $config;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->config = \Drupal::service('config.factory')->getEditable('site_commerce_import.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function createQueueCatalog() {
    $reader = new Xls();
    $spreadsheet = $reader->load($this->getFileCatalog());
    $sheet = $spreadsheet->getActiveSheet();
    $cells = $sheet->getCellCollection();
    $row_max = $cells->getHighestRow();

    // Строка с которой начнется импорт из файла.
    if (!$this->config->get('start_catalog_row')) {
      $row = 2;
    } else {
      $row = $this->config->get('start_catalog_row');
    }

    // Максимальное кол-во строк обрабатываемых за один цикл CRON;
    $row_chank_size = 5000;
    $row_chank_size_max = $row_chank_size + $row;

    $queue = \Drupal::queue('site_commerce_import_excel_catalog_import');
    $queue->createQueue();

    $data = [];
    $queue_created = TRUE;
    for ($row; $row <= $row_max; $row++) {
      if ($row > $row_chank_size_max) {
        $queue_created = FALSE;
        $this->config->set('start_catalog_row', $row)->save();
        \Drupal::logger('import_catalog')->notice('Import categories completed in row: @row.', ['@row' => $row]);
        break;
      }

      // Создаем очередь задания.
      $data = [
        'plugin_id' => $this->getPluginId(),
        'name' => $sheet->getCellByColumnAndRow(1, $row)->getCalculatedValue(),
        'code' => $sheet->getCellByColumnAndRow(2, $row)->getCalculatedValue(),
        'parent' => $sheet->getCellByColumnAndRow(3, $row)->getCalculatedValue(),
        'published' => $sheet->getCellByColumnAndRow(4, $row)->getCalculatedValue(),
      ];
      $queue->createItem($data);
    }

    // Записываем текущее значение md5. Считаем, что файл полностью обработан.
    if ($queue_created) {
      $this->config->set('file_catalog_md5', $this->getFileCatalogHash())->save();
      $this->config->set('start_catalog_row', 0)->save();
      \Drupal::logger('import_catalog')->notice('Import categories completed.');
    }

    // Системное уведомление о создании очереди.
    $this->messenger()->addMessage(
      $this->t('A queue has been created for importing categories: @value.', ['@value' => $queue->numberOfItems()])
    );
  }

  /**
   * {@inheritdoc}
   */
  public function createItemCatalog(array $data) {
    // Выполняем проверку обязательных ключей.
    if (!$this->checkDataCatalog($data)) {
      $message = $this->t('The category was not imported. Required keys were not checked. @value', [
        '@value' => json_encode($data, JSON_UNESCAPED_UNICODE),
      ]);
      \Drupal::logger('site_commerce_import')->error($message);
      return FALSE;
    }

    // Получаем данные и выполняем проверку.
    $name = $this->cleanTitle($data['name']);
    $field_code = trim($data['code']);
    $field_code_parent = trim($data['parent']);
    $field_view = (int) trim($data['published']);

    // Если указан артикул.
    if (!empty($field_code)) {
      $result = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['field_code' => ['value' => $field_code]]);
      $term = reset($result);

      // Если категория не найдена.
      if (!$term) {
        // Создаем новый термин.
        $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
        $term = [
          'name' => $name,
          'vid' => 'site_commerce_catalog',
          'langcode' => $langcode,
          'parent' => [0],
          'field_view' => [
            'value' => $field_view,
          ],
          'field_code' => [
            'value' => $field_code,
          ],
        ];

        // Определяем термин родитель.
        // TODO: Важно в файле импорта соблюдать порядок следования категорий, дочерние должны быть ниже!!!
        if (!empty($field_code_parent)) {
          $result = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['field_code' => ['value' => $field_code_parent]]);
          $term_parent = reset($result);
          if ($term_parent) {
            $term['parent'] = [$term_parent->id()];
          }
        }

        $term = Term::create($term);
        $term->save();
      } else {
        $term->name->setValue($name);
        $term->field_view->setValue(['value' => $field_view]);

        // Определяем термин родитель по артикулу.
        if (!empty($field_code_parent)) {
          $result = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['field_code' => ['value' => $field_code_parent]]);
          $term_parent = reset($result);
          if ($term_parent) {
            $term->parent->setValue([$term_parent->id()]);
          }
        }

        $term->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createQueueProducts() {
    $reader = new Xls();
    $spreadsheet = $reader->load($this->getFileProducts());
    $sheet = $spreadsheet->getActiveSheet();
    $cells = $sheet->getCellCollection();
    $row_max = $cells->getHighestRow();

    // Строка с которой начнется импорт из файла.
    if (!$this->config->get('start_products_row')) {
      $row = 2;
    } else {
      $row = $this->config->get('start_products_row');
    }

    // Максимальное кол-во строк обрабатываемых за один цикл CRON;
    // TODO: Нужно добавить эту настройку на форму.
    $row_chank_size = 10000;
    $row_chank_size_max = $row_chank_size + $row;

    $queue = \Drupal::queue('site_commerce_import_excel_products_import');
    $queue->createQueue();

    $data = [];
    $queue_created = TRUE;
    for ($row; $row <= $row_max; $row++) {
      if ($row > $row_chank_size_max) {
        $queue_created = FALSE;
        $this->config->set('start_products_row', $row)->save();
        \Drupal::logger('import_products')->notice('Import products completed in row: @row.', ['@row' => $row]);
        break;
      }

      // Создаем очередь задания.
      $data = [
        'plugin_id' => $this->getPluginId(),
        'type' => 'default', // TODO: нужно вынести в файл импорта в качестве первой колонки.
        'id_external' => $sheet->getCellByColumnAndRow(1, $row)->getCalculatedValue(),
        'code' => $sheet->getCellByColumnAndRow(2, $row)->getCalculatedValue(),
        'code_manufacturer' => $sheet->getCellByColumnAndRow(3, $row)->getCalculatedValue(),
        'category' => $sheet->getCellByColumnAndRow(4, $row)->getCalculatedValue(),
        'categories' => $sheet->getCellByColumnAndRow(5, $row)->getCalculatedValue(),
        'title' => $sheet->getCellByColumnAndRow(6, $row)->getCalculatedValue(),
        'summary' => $sheet->getCellByColumnAndRow(7, $row)->getCalculatedValue(),
        'quantity' => $sheet->getCellByColumnAndRow(8, $row)->getCalculatedValue(),
        'price' => $sheet->getCellByColumnAndRow(9, $row)->getCalculatedValue(),
        'currency_code' => $sheet->getCellByColumnAndRow(10, $row)->getCalculatedValue(),
        'quantity_unit' => $sheet->getCellByColumnAndRow(11, $row)->getCalculatedValue(),
        'status' => $sheet->getCellByColumnAndRow(12, $row)->getCalculatedValue(),
        'status_catalog' => $sheet->getCellByColumnAndRow(13, $row)->getCalculatedValue(),
        'images' => $sheet->getCellByColumnAndRow(14, $row)->getCalculatedValue(),
      ];
      $queue->createItem($data);
    }

    // Записываем текущее значение md5. Считаем, что файл полностью обработан.
    if ($queue_created) {
      $this->config->set('file_products_md5', $this->getFileProductsHash())->save();
      $this->config->set('start_products_row', 0)->save();
      \Drupal::logger('import_products')->notice('Import products processing completed.');
    }

    // Системное уведомление о создании очереди.
    $this->messenger()->addMessage(
      $this->t('A queue has been created for importing products: @value.', ['@value' => $queue->numberOfItems()])
    );
  }

  /**
   * {@inheritdoc}
   */
  public function createItemProduct(array $data) {
    // Создаваемая сущность и ее тип.
    // @param \Drupal\site_commerce_product\Entity\Product $entity
    $entity = NULL;
    $entity_type = 'site_commerce_product';
    $bundle = trim($data['type']);

    // Выполняем проверку обязательных ключей.
    if (!$this->checkDataProducts($data)) {
      $message = $this->t('The product was not imported. Required keys were not checked. @value', [
        '@value' => json_encode($data, JSON_UNESCAPED_UNICODE),
      ]);
      \Drupal::logger('site_commerce_import')->error($message);
      return FALSE;
    }

    // Получаем переменные.
    $field_id_external = empty($data['id_external']) ? 0 : trim($data['id_external']);
    $field_code = trim($data['code']);
    $field_code_manufacturer = trim($data['code_manufacturer']);
    $category = trim($data['category']);
    $title = $this->cleanTitle($data['title']);
    $field_summary_value = trim($data['summary']);

    // Цена товара и валюта.
    $field_price_group_number = (float) round(trim($data['price']), 2);
    $field_price_group_currency_code = trim($data['currency_code']);
    if (!preg_match('/^\d{3}$/i', $field_price_group_currency_code)) {
      $config = \Drupal::config('site_commerce_order.settings');
      $field_price_group_currency_code = $config->get('default_currency_code') ? $config->get('default_currency_code') : 'RUB';
    }

    // Количество на складе.
    $field_settings_quantity_stock_value = (int) trim($data['quantity']);
    $field_settings_quantity_unit = trim($data['quantity_unit']);

    // Статус публикации.
    $status = (int) trim($data['status']);
    $status_catalog = (int) trim($data['status_catalog']);

    $images = trim($data['images']);

    if ($field_id_external || $field_code) {
      if ($field_id_external) {
        $result = \Drupal::entityTypeManager()->getStorage($entity_type)->loadByProperties(['field_id_external' => $field_id_external]);
      } else {
        $result = \Drupal::entityTypeManager()->getStorage($entity_type)->loadByProperties(['field_code' => $field_code]);
      }
      $entity = reset($result);

      // Если товар не найден.
      if (empty($entity)) {
        // Создаем новый товар.
        $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();

        // Загружаем термин таксономии.
        // Если удалось определить категорию создаем товар.
        $categories_array = explode("|", $category);
        $name = array_pop($categories_array);
        $terms = taxonomy_term_load_multiple_by_name($name, 'site_commerce_catalog');
        $term = reset($terms);
        if ($term instanceof TermInterface) {
          $site_commerce_product = [
            'type' => $bundle,
            'title' => $title,
            'langcode' => $langcode,
            'parent' => 0,
            'uid' => 1,
            'status' => $status,
            'status_catalog' => $status_catalog,
            'field_id_external' => [$field_id_external],
            'field_summary' => [
              'value' => $field_summary_value,
              'format' => 'basic_html',
            ],
            'field_category' => [
              'target_id' => $term->id(),
            ],
            'field_code' => [$field_code],
            'field_code_manufacturer' => [$field_code_manufacturer],
            'field_price_group' => [
              'group' => 'retail',
              'prefix' => '',
              'suffix' => '',
              'number_from' => 0,
              'number' => $field_price_group_number,
              'currency_code' => $field_price_group_currency_code,
            ],
            'field_settings' => [
              'status_new' => 0,
              'status_appearance' => 0,
              'status_stock' => 0,
              'status_type_sale' => 1,
              'quantity_stock_allow' => $field_settings_quantity_stock_value ? 1 : 0,
              'quantity_stock_value' => $field_settings_quantity_stock_value,
              'quantity_order_min' => 1,
              'quantity_order_max' => 0,
              'quantity_unit' => $field_settings_quantity_unit,
              'cart_form_allow' => 1,
              'cart_quantity_allow' => 1,
              'cart_after_adding_event' => 'load_goto_order_checkout_form',
              'cart_label_button_add' => '',
              'cart_label_button_click' => '',
              'cart_description' => '',
            ],
          ];

          // Создаем папку для хранения изображений товаров.
          $images_array = explode("|", $images);
          $field_media_image = [];
          foreach ($images_array as $image) {
            $path = 'public://import/images/' . $image;
            if ($image && file_exists($path)) {
              $file_contents = file_get_contents($path);
              if ($file_contents) {
                $pathinfo = pathinfo($path);

                // Формирует имя файла.
                $path = \Drupal::service('pathauto.alias_cleaner')->cleanstring($title);
                $path = \Drupal::transliteration()->transliterate($path, $langcode);
                $filename = $path . '.' . $pathinfo['extension'];

                // Удаление пробелов.
                $filename = str_replace(' ', '-', $filename);

                // Удаление не безопасных символов.
                $filename = preg_replace('![^0-9A-Za-z_.-]!', '', $filename);

                // Удаление символов не алфавита.
                $filename = preg_replace('/(_)_+|(\.)\.+|(-)-+/', '\\1\\2\\3', $filename);

                // Преобразование.
                $filename = mb_strtolower($filename);

                $directory = 'public://images/products/' . date('Y') . '-' . date('m');
                $destination = $directory . '/' . $filename;

                // Проверка длины пути файла не более 255 символов.
                if (mb_strlen($destination) > 255) {
                  $pathinfo = pathinfo($destination);
                  $destination = mb_substr($destination, 0, 254 - mb_strlen($pathinfo['extension'])) . ".{$pathinfo['extension']}";
                }

                \Drupal::service('file_system')->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
                $file = file_save_data($file_contents, $destination, FileSystemInterface::EXISTS_REPLACE);
                if ($file instanceof File) {
                  // Выполняем проверку на поиск дубликата файла.
                  // Проверяем есть ли дубликаты медиаобьекта, чтобы не создавать его повторно
                  // с одним и тем же файлом.
                  $file = \Drupal::service('kvantstudio.validator')->fileHasDuplicate($file);
                  $media = NULL;
                  if ($file->has_dublicate) {
                    $result = \Drupal::entityTypeManager()->getStorage('media')->loadByProperties(['field_media_image.target_id' => $file->id()]);
                    $media = reset($result);
                  }

                  // Создаем медиа объект и прикрепляем к нему файл.
                  if (!$media) {
                    $media = Media::create([
                      'bundle' => 'product_image',
                      'uid' => '1',
                      'langcode' => $langcode,
                      'status' => 1,
                      'field_media_image' => [
                        'target_id' => $file->id(),
                        'alt' => $title,
                        'title' => $title,
                      ],
                    ]);
                    $media->setName($title);
                    $media->save();
                  }

                  // Добавляем созданный медиа объект в массив для привязки к объекту товара к полю field_media_image.
                  if ($media instanceof Media) {
                    $field_media_image[] = [
                      'target_id' => $media->id(),
                    ];
                  }
                }
              }
            }
          }

          // Если созданы медиа объекты изображений.
          if (count($field_media_image)) {
            $site_commerce_product['field_media_image'] = $field_media_image;
          }

          // Сохраняем товар.
          $entity = \Drupal::entityTypeManager()->getStorage($entity_type)->create($site_commerce_product);
          $entity->enforceIsNew(TRUE);
          $entity->save();
        } else {
          $message = $this->t('The product was not imported. The @value product category was not created on the site.', ['@value' => $name]);
          \Drupal::logger('site_commerce_import')->error($message);
        }
      } else {
        $entity->title->setValue($title);
        $entity->status->setValue($status);
        $entity->field_summary->setValue(['value' => $field_summary_value]);
        $entity->field_code->setValue(['value' => $field_code]);
        $entity->field_price_group->setValue([
          'group' => 'retail',
          'number' => $field_price_group_number,
          'currency_code' => $field_price_group_currency_code,
        ]);

        $entity->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createQueueCatalogUrl() {
    $reader = new Xls();
    $spreadsheet = $reader->load($this->getFileCatalogUrl());
    $sheet = $spreadsheet->getActiveSheet();
    $cells = $sheet->getCellCollection();
    $row_max = $cells->getHighestRow();

    // Строка с которой начнется импорт из файла.
    if (!$this->config->get('start_catalog_url_row')) {
      $row = 2;
    } else {
      $row = $this->config->get('start_catalog_url_row');
    }

    // Максимальное кол-во строк обрабатываемых за один цикл CRON;
    $row_chank_size = 5000;
    $row_chank_size_max = $row_chank_size + $row;

    $queue = \Drupal::queue('site_commerce_import_excel_catalog_url_import');
    $queue->createQueue();

    $data = [];
    $queue_created = TRUE;
    for ($row; $row <= $row_max; $row++) {
      if ($row > $row_chank_size_max) {
        $queue_created = FALSE;
        $this->config->set('start_catalog_url_row', $row)->save();
        \Drupal::logger('import_catalog_url')->notice('Import URL links of categories completed in row: @row.', ['@row' => $row]);
        break;
      }

      // Создаем очередь задания.
      $data = [
        'plugin_id' => $this->getPluginId(),
        'code' => $sheet->getCellByColumnAndRow(1, $row)->getCalculatedValue(),
        'alias' => $sheet->getCellByColumnAndRow(2, $row)->getCalculatedValue(),
      ];
      $queue->createItem($data);
    }

    // Выполняем если файл полностью обработан.
    if ($queue_created) {
      $this->config->set('start_catalog_url_row', 0)->save();
      \Drupal::logger('import_catalog_url')->notice('Import URL links of categories completed.');
    }

    // Системное уведомление о создании очереди.
    $this->messenger()->addMessage(
      $this->t('A queue has been created for importing URL links of categories: @value.', ['@value' => $queue->numberOfItems()])
    );
  }

  /**
   * {@inheritdoc}
   */
  public function createItemCatalogURL(array $data) {
    // Получаем данные и выполняем проверку.
    $field_code = trim($data['code']);
    $field_alias = trim($data['alias']);

    // Если указан артикул и алиас.
    if (!empty($field_code) && !empty($field_alias)) {
      $result = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['field_code' => ['value' => $field_code]]);
      $term = reset($result);
      if ($term instanceof TermInterface) {
        $path = '/taxonomy/term/' . $term->id();
        $alias = '/' . $field_alias;

        $pathStorage = \Drupal::entityTypeManager()->getStorage('path_alias');
        $result = $pathStorage->loadByProperties(['path' => $path]);
        $path_alias = reset($result);
        if ($path_alias instanceof PathAliasInterface) {
          $path_alias->setAlias($alias);
        } else {
          $path_alias = \Drupal\path_alias\Entity\PathAlias::create([
            'path' => $path,
            'alias' => $alias,
          ]);
        }

        $path_alias->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createQueueProductsUrl() {
    $reader = new Xls();
    $spreadsheet = $reader->load($this->getFileProductsUrl());
    $sheet = $spreadsheet->getActiveSheet();
    $cells = $sheet->getCellCollection();
    $row_max = $cells->getHighestRow();

    // Строка с которой начнется импорт из файла.
    if (!$this->config->get('start_product_url_row')) {
      $row = 2;
    } else {
      $row = $this->config->get('start_product_url_row');
    }

    // Максимальное кол-во строк обрабатываемых за один цикл CRON;
    $row_chank_size = 5000;
    $row_chank_size_max = $row_chank_size + $row;

    $queue = \Drupal::queue('site_commerce_import_excel_product_url_import');
    $queue->createQueue();

    $data = [];
    $queue_created = TRUE;
    for ($row; $row <= $row_max; $row++) {
      if ($row > $row_chank_size_max) {
        $queue_created = FALSE;
        $this->config->set('start_product_url_row', $row)->save();
        \Drupal::logger('import_product_url')->notice('Import URL links of products completed in row: @row.', ['@row' => $row]);
        break;
      }

      // Создаем очередь задания.
      $data = [
        'plugin_id' => $this->getPluginId(),
        'code' => $sheet->getCellByColumnAndRow(1, $row)->getCalculatedValue(),
        'alias' => $sheet->getCellByColumnAndRow(2, $row)->getCalculatedValue(),
      ];
      $queue->createItem($data);
    }

    // Выполняем если файл полностью обработан.
    if ($queue_created) {
      $this->config->set('start_product_url_row', 0)->save();
      \Drupal::logger('import_product_url')->notice('Import URL links of products completed.');
    }

    // Системное уведомление о создании очереди.
    $this->messenger()->addMessage(
      $this->t('A queue has been created for importing URL links of products: @value.', ['@value' => $queue->numberOfItems()])
    );
  }

  /**
   * {@inheritdoc}
   */
  public function createItemProductUrl(array $data) {
    // Получаем данные и выполняем проверку.
    $field_code = trim($data['code']);
    $field_alias = trim($data['alias']);

    // Если указан артикул и алиас.
    if (!empty($field_code) && !empty($field_alias)) {
      $result = \Drupal::entityTypeManager()->getStorage('site_commerce_product')->loadByProperties(['field_code' => ['value' => $field_code]]);
      $product = reset($result);
      if ($product instanceof ProductInterface) {
        $path = '/product/' . $product->id();
        $alias = '/' . $field_alias;

        $pathStorage = \Drupal::entityTypeManager()->getStorage('path_alias');
        $result = $pathStorage->loadByProperties(['path' => $path]);
        $path_alias = reset($result);
        if ($path_alias instanceof PathAliasInterface) {
          $path_alias->setAlias($alias);
        } else {
          $path_alias = \Drupal\path_alias\Entity\PathAlias::create([
            'path' => $path,
            'alias' => $alias,
          ]);
        }

        $path_alias->save();
      }
    }
  }
}

<?php

namespace Drupal\site_commerce_import;

use Drupal\Component\Plugin\PluginInspectionInterface;

interface ExcelPluginInterface extends PluginInspectionInterface {

  /**
   * Реализует получение идентификатора плагина.
   * @return mixed
   */
  public function getId();

  /**
   * Реализует получение названия плагина.
   * @return mixed
   */
  public function getLabel();

  /**
   * Реализует получение пути на файл Excel с категориями товаров.
   * @return mixed
   */
  public function getFileCatalog();

  /**
   * Реализует вычисление HASH суммы файла Excel с категориями товаров.
   * @return mixed
   */
  public function getFileCatalogHash();

  /**
   * Реализует получение пути на файл Excel с товарами.
   * @return mixed
   */
  public function getFileProducts();

  /**
   * Реализует вычисление HASH суммы файла Excel с товарами.
   * @return mixed
   */
  public function getFileProductsHash();

  /**
   * Реализует создание очереди импорта категорий товаров.
   * @return mixed
   */
  public function createQueueCatalog();

  /**
   * Реализует создание очереди импорта товаров.
   * @return mixed
   */
  public function createQueueProducts();

  /**
   * Реализует создание категории товара в БД.
   * @param array $data
   * @return mixed
   */
  public function createItemCatalog(array $data);

  /**
   * Реализует проверку на заполнение минимально необходимых полей при импорте категории товара.
   * @param array $data
   * @return mixed
   */
  public function checkDataCatalog(array $data);

  /**
   * Реализует создание товара в БД.
   * @param array $data
   * @return mixed
   */
  public function createItemProduct(array $data);

  /**
   * Реализует проверку на заполнение минимально необходимых полей при импорте товара.
   * @param array $data
   * @return mixed
   */
  public function checkDataProducts(array $data);

  /**
   * Реализует очистку спецсимволов у заголовка товара или категории перед занесением в БД.
   * @param string $title
   * @return mixed
   */
  public function cleanTitle(string $title);

  /**
   * Реализует получение пути на файл Excel с URL ссылками каталога товаров.
   * @return mixed
   */
  public function getFileCatalogUrl();

  /**
   * Реализует создание очереди импорта URL ссылок каталога товаров.
   * @return mixed
   */
  public function createQueueCatalogUrl();

  /**
   * Реализует создание URL ссылки категории каталога товаров в БД.
   * @param array $data
   * @return mixed
   */
  public function createItemCatalogUrl(array $data);

  /**
   * Реализует получение пути на файл Excel с URL ссылками товаров.
   * @return mixed
   */
  public function getFileProductsUrl();

  /**
   * Реализует создание очереди импорта URL ссылок товаров.
   * @return mixed
   */
  public function createQueueProductsUrl();

  /**
   * Реализует создание URL ссылки товара в БД.
   * @param array $data
   * @return mixed
   */
  public function createItemProductUrl(array $data);
}

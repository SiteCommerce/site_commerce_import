<?php

namespace Drupal\site_commerce_import\Plugin\QueueWorker;

use Drupal\Core\Annotation\QueueWorker;
use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Processes products import.
 *
 * @QueueWorker(
 *   id = "site_commerce_import_excel_products_import",
 *   title = @Translation("SiteCommerce: import products from Excel"),
 *   cron = {"time" = 60}
 * )
 */
class ExcelProductsImport extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $importPlugin = \Drupal::service('plugin.manager.site_commerce_import.excel')->createInstance($data['plugin_id']);
    $importPlugin->createItemProduct($data);
  }

}

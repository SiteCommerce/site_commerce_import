<?php

namespace Drupal\site_commerce_import;

use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides an Excel plugin manager for import of categories and products.
 */
class ExcelPluginManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/SiteCommerceImport',
      $namespaces,
      $module_handler,
      'Drupal\site_commerce_import\ExcelPluginInterface',
      'Drupal\site_commerce_import\Annotation\ExcelPlugin'
    );

    $this->alterInfo('site_commerce_import_excel_plugin_info');
    $this->setCacheBackend($cache_backend, 'site_commerce_import_excel_plugin');
    $this->factory = new DefaultFactory($this->getDiscovery());
  }

}

<?php

namespace Drupal\site_commerce_import\Plugin\rest\resource\v1;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\redirect\Entity\Redirect;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\site_commerce_product\Entity\Product;
use Drupal\taxonomy\Entity\Term;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Реализует импорт товаров.
 *
 * @RestResource(
 *   id = "site_commerce_import_products",
 *   label = @Translation("SiteCommerce: import products by API"),
 *   uri_paths = {
 *     "create" = "/api/v1/site-commerce-import/products",
 *   }
 * )
 */
class ImportProducts extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new ImportProducts object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('site_referral_program'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to POST requests.
   */
  public function post($data) {
    try {
      $response['result'] = NULL;

      // Создаваемый объект.
      // @param \Drupal\site_commerce_product\Entity\Product $entity
      $entity = NULL;
      $entity_type = 'site_commerce_product';
      $bundle = 'default';

      // Получаем исходные данные.
      $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
      $alias_product = trim($data['alias_product'], '/');
      $alias_category = trim($data['alias_category'], '/');
      $metatags = $data['metatags'];
      $cart_description = trim($data['cart_description']);
      $category = trim($data['category']);
      $title = trim($data['title']);
      $code = trim($data['code']);
      $number_from = (float) $data['number_from'];
      $number = (float) $data['number'];
      $unit = trim($data['unit']);
      $summary = trim($data['summary']);
      $summary_catalog = trim($data['summary_catalog']);
      $content = trim($data['content']);

      // Получаем объект категории товара.
      $terms = taxonomy_term_load_multiple_by_name($category, 'site_commerce_catalog');
      $term = reset($terms);
      if ($term instanceof Term) {

        // Устанавливаем редирект на старый алиас сущности категории товаров.
        if ($alias_category) {
          // Проверяем наличие редиректа.
          $query = \Drupal::entityTypeManager()->getStorage('redirect')->getQuery();
          $query->condition('redirect_redirect.uri', 'internal:/taxonomy/term/' . $term->id());
          $redirect_ids = $query->execute();
          if (!$redirect_ids) {
            Redirect::create([
              'redirect_source' => $alias_category,
              'redirect_redirect' => 'internal:/taxonomy/term/' . $term->id(),
              'language' => $langcode,
              'status_code' => 301,
            ])->save();
          }

          // NOTE: вариант установки алиаса для категории. Оставлено для примера.
          // $pathStorage = \Drupal::entityTypeManager()->getStorage('path_alias');
          // $aliases = $pathStorage->loadByProperties(['alias' => $alias_category]);
          // if (!count($aliases)) {
          //   $path_alias = \Drupal\path_alias\Entity\PathAlias::create([
          //     'path' => '/taxonomy/term/' . $term->id(),
          //     'alias' => $alias_category,
          //   ]);

          //   $path_alias->save();
          // }
        }

        // Общие параметры товара.
        $site_commerce_product = [
          'type' => $bundle,
          'title' => $title,
          'langcode' => $langcode,
          'parent' => 0,
          'uid' => 1,
          'status' => 1,
          'status_catalog' => 1,
          'field_category' => [
            'target_id' => $term->id(),
          ],
          'field_code' => [
            'value' => $code,
          ],
          'field_price_group' => [
            'group' => 'retail',
            'prefix' => '',
            'suffix' => '',
            'number_from' => $number_from,
            'number' => $number,
            'currency_code' => 'RUB',
          ],
          'field_summary' => [
            'value' => $summary,
            'format' => 'basic_html',
          ],
          'field_summary_catalog' => [
            'value' => $summary_catalog,
            'format' => 'basic_html',
          ],
          'field_settings' => [
            'status_new' => 0,
            'status_appearance' => 0,
            'status_stock' => 0,
            'status_type_sale' => 1,
            'quantity_stock_allow' => 0,
            'quantity_stock_value' => 0,
            'quantity_order_min' => 1,
            'quantity_order_max' => 0,
            'quantity_unit' => $unit,
            'cart_form_allow' => 1,
            'cart_quantity_allow' => 1,
            'cart_after_adding_event' => 'load_goto_order_checkout_form',
            'cart_label_button_add' => 'В корзину',
            'cart_label_button_click' => 'Быстрый заказ',
            'cart_description' => $cart_description,
          ],
        ];

        // Если передано содержимое.
        if ($content) {
          $paragraph = Paragraph::create([
            'type' => 'text',
            'field_body' => [
              'value' => $content,
              'format' => 'basic_html',
            ],
          ]);
          $paragraph->save();

          if ($paragraph instanceof Paragraph) {
            $field_content[] = [
              'target_id' => $paragraph->id(),
              'target_revision_id' => $paragraph->getRevisionId(),
            ];
            $site_commerce_product['field_content'] = $field_content;
          }
        }

        // Если переданы метатеги.
        $field_meta_tags = 'field_meta_tags';
        $bundle_have_field = \Drupal::service('kvantstudio.validator')->doesBundleHaveField($entity_type, $bundle, $field_meta_tags);
        if ($bundle_have_field && count($metatags)) {
          $site_commerce_product[$field_meta_tags] = serialize([
            'title' => isset($metatags['title']) ? trim($metatags['title']) : '',
            'description' => isset($metatags['description']) ? trim($metatags['description']) : '',
            'keywords' => isset($metatags['keywords']) ? trim($metatags['keywords']) : '',
          ]);
        }

        // Создает медиа сущности изображения товара.
        $field_media_image = [];
        foreach ($data['images'] as $image) {

          $image_url = $image['url'];
          $arrContextOptions = [
            "ssl" => [
              "verify_peer" => false,
              "verify_peer_name" => false,
            ],
          ];
          $file_contents = file_get_contents($image_url, false, stream_context_create($arrContextOptions));
          if ($file_contents) {
            $pathinfo = pathinfo($image_url);

            // Формирует имя файла.
            $path = \Drupal::service('pathauto.alias_cleaner')->cleanstring($title);
            $path = \Drupal::transliteration()->transliterate($path, $langcode);
            $filename = $path . '.' . $pathinfo['extension'];

            // Удаление пробелов.
            $filename = str_replace(' ', '-', $filename);

            // Удаление не безопасных символов.
            $filename = preg_replace('![^0-9A-Za-z_.-]!', '', $filename);

            // Удаление символов не алфавита.
            $filename = preg_replace('/(_)_+|(\.)\.+|(-)-+/', '\\1\\2\\3', $filename);

            // Преобразование.
            $filename = Unicode::strtolower($filename);

            $directory = 'public://images/products/' . date('Y') . '-' . date('m');
            $destination = $directory . '/' . $filename;

            // Проверка длины пути файла не более 255 символов.
            if (Unicode::strlen($destination) > 255) {
              $pathinfo = pathinfo($destination);
              $destination = Unicode::substr($destination, 0, 254 - Unicode::strlen($pathinfo['extension'])) . ".{$pathinfo['extension']}";
            }

            \Drupal::service('file_system')->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
            $file = file_save_data($file_contents, $destination, FileSystemInterface::EXISTS_REPLACE);
            if ($file instanceof File) {
              // Выполняем проверку на поиск дубликата файла.
              // Проверяем есть ли дубликаты медиаобьекта, чтобы не создавать его повторно
              // с одним и тем же файлом.
              $file = \Drupal::service('kvantstudio.validator')->fileHasDuplicate($file);
              $media = NULL;
              if ($file->has_dublicate) {
                $result = \Drupal::entityTypeManager()->getStorage('media')->loadByProperties(['field_media_image.target_id' => $file->id()]);
                $media = reset($result);
              }

              // Создаем медиа объект и прикрепляем к нему файл.
              if (!$media) {
                $media = Media::create([
                  'bundle' => 'product_image',
                  'uid' => '1',
                  'langcode' => $langcode,
                  'status' => 1,
                  'field_media_image' => [
                    'target_id' => $file->id(),
                    'alt' => empty($image['alt']) ? $title : $image['alt'],
                    'title' => $image['title'],
                  ],
                ]);
                $media->setName($title);
                $media->save();
              }

              if ($media instanceof Media) {
                $field_media_image[] = [
                  'target_id' => $media->id(),
                ];

              }
            }
          }
        }

        // Если созданы медиа объекты изображений.
        if ($field_media_image) {
          $site_commerce_product['field_media_image'] = $field_media_image;
        }

        $entity = \Drupal::entityTypeManager()->getStorage($entity_type)->create($site_commerce_product);
        $entity->enforceIsNew(TRUE);
        $entity->save();
      } else {
        $response['status'] = FALSE;
        return new ResourceResponse("Категория товара $category не найдена.", 400);
      }

      // Если товар успешно создан.
      if ($entity instanceof Product) {

        // Устанавливаем редирект на старый алиас сущности товара.
        if ($alias_product) {
          // Проверяем наличие редиректа.
          $query = \Drupal::entityTypeManager()->getStorage('redirect')->getQuery();
          $query->condition('redirect_source.path', $alias_product);
          $redirect_ids = $query->execute();
          if (!$redirect_ids) {
            Redirect::create([
              'redirect_source' => $alias_product,
              'redirect_redirect' => 'internal:/product/' . $entity->id(),
              'language' => $langcode,
              'status_code' => 301,
            ])->save();
          }

          // NOTE: вариант установки алиаса для товара. Оставлено для примера.
          // // Проверяем алиас на уникальность.
          // $pathStorage = \Drupal::entityTypeManager()->getStorage('path_alias');
          // $aliases = $pathStorage->loadByProperties(['alias' => $alias_product]);
          // if (count($aliases)) {
          //   $alias_product = $alias_product . '-' . $entity->id();
          // }

          // // Загружаем алиас созданного товара.
          // $path_alias = reset($pathStorage->loadByProperties(['path' => '/site-commerce-product/' . $entity->id()]));
          // if ($path_alias) {
          //   $path_alias->setAlias($alias_product);
          // } else {
          //   $path_alias = \Drupal\path_alias\Entity\PathAlias::create([
          //     'path' => '/site-commerce-product/' . $entity->id(),
          //     'alias' => $alias_product,
          //   ]);
          // }

          // $path_alias->save();
        }

        $response['status'] = TRUE;
        $response['result']['product'] = $entity->id();

        return new ModifiedResourceResponse($response);
      } else {
        $response['status'] = FALSE;
        return new ResourceResponse($response, 400);
      }
    } catch (\Exception $e) {
      return new ResourceResponse('Произошла ошибка при импорте товара.', 400);
    }
  }
}

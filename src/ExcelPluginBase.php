<?php

namespace Drupal\site_commerce_import;

use Drupal\Component\Plugin\PluginBase;

/** @package Drupal\site_commerce_import */
abstract class ExcelPluginBase extends PluginBase implements ExcelPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function getId() {
    return $this->pluginDefinition['id'];
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFileCatalog() {
    $file = \Drupal::config('site_commerce_import.settings')->get('file_catalog_path');
    return $file;
  }

  /**
   * {@inheritdoc}
   */
  public function getFileCatalogUrl() {
    $file = \Drupal::config('site_commerce_import.settings')->get('file_catalog_url_path');
    return $file;
  }

  /**
   * {@inheritdoc}
   */
  public function getFileCatalogHash() {
    return md5_file($this->getFileCatalog());
  }

  /**
   * {@inheritdoc}
   */
  public function getFileProducts() {
    $file = \Drupal::config('site_commerce_import.settings')->get('file_products_path');
    return $file;
  }

  /**
   * {@inheritdoc}
   */
  public function getFileProductsUrl() {
    $file = \Drupal::config('site_commerce_import.settings')->get('file_products_url_path');
    return $file;
  }

  /**
   * {@inheritdoc}
   */
  public function getFileProductsHash() {
    return md5_file($this->getFileProducts());
  }

  /**
   * {@inheritdoc}
   */
  public function checkDataCatalog(array $data) {
    $fields = [
      'plugin_id',
      'name',
      'code',
    ];

    foreach ($fields as $field) {
      if (empty($data[$field])) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function checkDataProducts(array $data) {
    $fields = [
      'plugin_id',
      'code',
      'category',
      'title'
    ];

    foreach ($fields as $field) {
      if (empty($data[$field])) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function cleanTitle(string $title) {
    $title = trim($title);
    return str_replace('"', '»', preg_replace('/((^|\s)"(\w))/um', '\2«\3', $title));
  }

}
